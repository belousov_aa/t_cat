#include "json_builder.h"

namespace json {
 
ArrayContext& ArrayContext::Value(Node::Value value) {
  builder_ptr_->Value(std::move(value));
  return *this;
}

ArrayContext& ArrayContext::StartArray() {
  builder_ptr_->StartArray();
  return *this;
}

DictContext ArrayContext::StartDict() {
  builder_ptr_->StartDict();
  return DictContext(builder_ptr_);
}

Builder& ArrayContext::EndArray() {
  builder_ptr_->EndArray();
  return *builder_ptr_;
}

KeyContext DictContext::Key(std::string key) {
  builder_ptr_->Key(std::move(key));
  return KeyContext(builder_ptr_);
}

Builder& DictContext::EndDict() {
  builder_ptr_->EndDict();
  return *builder_ptr_;
}
   
DictContext KeyContext::Value(Node::Value value) {
  builder_ptr_->Value(std::move(value));
  return DictContext(builder_ptr_);
}

ArrayContext KeyContext::StartArray() {
  builder_ptr_->StartArray();
  return ArrayContext(builder_ptr_);
}

DictContext KeyContext::StartDict() {
  builder_ptr_->StartDict();
  return DictContext(builder_ptr_);
}

Node Builder::Build() const {
  if (!node_is_complete_)
    throw std::logic_error("Error appeared during Node building");
  return root_;
}

KeyContext Builder::Key(std::string key) {
  if (!key_is_expected_)
    throw std::logic_error("Key is not expected here");
  keys_.emplace(std::move(key));
  key_is_expected_ = false;
  return KeyContext(this);
}

Builder& Builder::Value(Node::Value value) {
  if (key_is_expected_ || node_is_complete_)
    throw std::logic_error("Value is prohibited here");
    
  if (nested_containers_.empty()) {
    root_ = GetVariantValue(std::move(value));
    node_is_complete_ = true;
  } else if (nested_containers_.top() == ConType::ARRAY) 
    arrays_.top().emplace_back(GetVariantValue(std::move(value)));
  else {
    dicts_.top()[std::move(keys_.top())] = GetVariantValue(std::move(value));
    keys_.pop();
    key_is_expected_ = true;
  } 
  return *this;
}
  
ArrayContext Builder::StartArray() {
  if (key_is_expected_ || node_is_complete_)
    throw std::logic_error("StartArray() is prohibited here");
  nested_containers_.emplace(ConType::ARRAY);
  arrays_.emplace(Array());
  return ArrayContext(this);
}

DictContext Builder::StartDict() {
  if (key_is_expected_ || node_is_complete_)
    throw std::logic_error("StartDict() is prohibited here");
  nested_containers_.emplace(ConType::DICT);
  dicts_.emplace(Dict());
  key_is_expected_ = true;
  return DictContext(this);
}

Builder& Builder::EndArray() {
  if (key_is_expected_ ||
      nested_containers_.empty() ||
      nested_containers_.top() == ConType::DICT)
    throw std::logic_error("EndArray is prohibited here");

  nested_containers_.pop();
  Array tmp_array = std::move(arrays_.top());
  arrays_.pop();

  if (nested_containers_.empty()) {
    root_ = std::move(tmp_array);
    node_is_complete_ = true;
  } else  if (nested_containers_.top() == ConType::ARRAY) {
    arrays_.top().emplace_back(std::move(tmp_array));
    key_is_expected_ = false;  
  } else if (nested_containers_.top() == ConType::DICT)  { 
    dicts_.top()[std::move(keys_.top())] = std::move(tmp_array);
    keys_.pop();
    key_is_expected_ = true;
  }
  return *this;
}


Builder& Builder::EndDict() {
  if (nested_containers_.empty() ||
      nested_containers_.top() == ConType::ARRAY)
    throw std::logic_error("EndDict is prohibited here");

  nested_containers_.pop();
  Dict tmp_dict = std::move(dicts_.top());
  dicts_.pop();
  key_is_expected_ = false;
    
  if (nested_containers_.empty()) {
    root_ = std::move(tmp_dict);
    node_is_complete_ = true;
  } else  if (nested_containers_.top() == ConType::ARRAY) 
    arrays_.top().emplace_back(std::move(tmp_dict));
  else if (nested_containers_.top() == ConType::DICT)  { 
    dicts_.top()[std::move(keys_.top())] = std::move(tmp_dict);
    keys_.pop();
    key_is_expected_ = true;
  }   
  return *this;
}
    
Node Builder::GetVariantValue(Node::Value&& value) {
  if (std::holds_alternative<nullptr_t>(value)) 
    return std::move(std::get<nullptr_t>(value));
  if (std::holds_alternative<Dict>(value)) 
    return std::move(std::get<Dict>(value));
  if (std::holds_alternative<Array>(value)) 
    return std::move(std::get<Array>(value));  
  if (std::holds_alternative<std::string>(value)) 
    return std::move(std::get<std::string>(value));
  if (std::holds_alternative<double>(value)) 
    return std::move(std::get<double>(value));
  return std::move(std::get<int>(value));      
}

} // namespace json
