#include "json_reader.h"

#include <cassert>
#include <charconv>
#include <iostream>
#include <string>
#include <string_view>
#include <unordered_set>
#include <vector>

namespace transcat {
namespace input_reader {

using namespace std::literals;

InputReader::InputReader(TransportCatalogue& catalogue)
    : catalogue_(catalogue)
{}

JSONReader::JSONReader(TransportCatalogue& catalogue,
                       std::istream& input)
    : InputReader(catalogue)
    , doc_(json::Load(input))
{
  ReadBaseRequests();
  ReadRenderSettings();
  ReadStatRequests();
}

map_renderer::MapSettings  JSONReader::GetSettings() const noexcept {
  return settings_;
}
std::deque<domain::Stats> JSONReader::GetStats() const noexcept {
  return stats_;
}

void JSONReader::ReadBaseRequests() {
  if (doc_.GetRoot().AsMap().find("base_requests"s) ==
      doc_.GetRoot().AsMap().end())
    return;
    
  const json::Node& requests = doc_.GetRoot().AsMap().at("base_requests"s);
  const std::string type{"type"s}, stop{"Stop"s}, bus{"Bus"s};
  
  std::vector<json::Node> buses;

  for (const json::Node& request : requests.AsArray()) {
    if (request.AsMap().at(type) == stop) {
      StopToCatalogue(request);
      continue;
    }
    if (request.AsMap().at(type) == bus)
      buses.emplace_back(request);
  }
  for (const json::Node& request : buses)
    RouteToCatalogue(request);
}

void JSONReader::ReadRenderSettings() {
  using namespace std::literals;
  
  if (doc_.GetRoot().AsMap().find("render_settings"s) ==
      doc_.GetRoot().AsMap().end())
    return;
  
  const json::Dict& request {doc_.GetRoot().AsMap().at("render_settings"s).AsMap()};

  if (request.find("width"s) != request.end())
    settings_.SetWidth(request.at("width"s).AsDouble());
  if (request.find("height"s) != request.end())
    settings_.SetHeight(request.at("height"s).AsDouble());
  if (request.find("padding"s) != request.end())
    settings_.SetPadding(request.at("padding"s).AsDouble());
  if (request.find("line_width"s) != request.end())
    settings_.SetLineWidth(request.at("line_width"s).AsDouble());
  if (request.find("stop_radius"s) != request.end())
    settings_.SetStopRadius(request.at("stop_radius"s).AsDouble());
  if (request.find("bus_label_font_size"s) != request.end())
    settings_.SetBusLabelFontSize(request.at("bus_label_font_size"s).AsInt());
  if (request.find("stop_label_font_size"s) != request.end())
    settings_.SetStopLabelFontSize(request.at("stop_label_font_size"s).AsInt());
  if (request.find("underlayer_width"s) != request.end())
    settings_.SetUnderlayerWidth(request.at("underlayer_width"s).AsDouble());
  if (request.find("bus_label_offset"s) != request.end())
    settings_.SetBusLabelOffset(
        {request.at("bus_label_offset"s).AsArray().at(0).AsDouble(),
         request.at("bus_label_offset"s).AsArray().at(1).AsDouble()});
  if (request.find("stop_label_offset"s) != request.end())
    settings_.SetStopLabelOffset(
        {request.at("stop_label_offset"s).AsArray().at(0).AsDouble(),
         request.at("stop_label_offset"s).AsArray().at(1).AsDouble()});
  if (request.find("underlayer_color"s) != request.end())
    settings_.SetUnderlayerColor(MakeColor(request.at("underlayer_color"s)));
  if (request.find("color_palette"s) != request.end())
    settings_.SetColorPalette(MakePalette(request.at("color_palette"s)));
}    
    
void JSONReader::ReadStatRequests() {
  if (doc_.GetRoot().AsMap().find("stat_requests"s) ==
      doc_.GetRoot().AsMap().end())
    return;
    
  stats_.clear();
    
  const json::Array& requests = doc_.GetRoot().AsMap().at("stat_requests"s).AsArray();
  const std::string
      id    {"id"s},  name  {"name"s}, type   {"type"s},
      map_t {"Map"s}, bus_t {"Bus"s},  stop_t {"Stop"s};
    
  for (const json::Node& request : requests) {
    domain::Stats stat;
    const json::Dict& dict = request.AsMap();
    stat.id = dict.at(id).AsInt();
    if (dict.at(type).AsString() == map_t) {
      stat.type = domain::Stats::Type::Map;
      stats_.emplace_back(std::move(stat));
      continue;
    }
    if (dict.at(type).AsString() == bus_t)
      stat.type = domain::Stats::Type::Bus;
    else if (dict.at(type).AsString() == stop_t)
      stat.type = domain::Stats::Type::Stop;
    else
      continue;
    stat.name = dict.at(name).AsString();
    stats_.emplace_back(std::move(stat));
  }
}

void JSONReader::RouteToCatalogue(const json::Node& bus_node) {
  std::string name  {bus_node.AsMap().at("name"s).AsString()};
  const bool is_roundtrip {bus_node.AsMap().at("is_roundtrip"s).AsBool()};
  std::vector<domain::Stop*> bus_route;
  std::unordered_set<domain::Stop*, domain::PtrHasher> u_stops; 

  const json::Array& stops = bus_node.AsMap().at("stops"s).AsArray();
  const bool first_and_last_stops_are_equal = (stops.empty())
      ? true
      : stops.front().AsString() == stops.back().AsString();

  for (const json::Node& stop_name : stops) {
    domain::Stop* stop_ptr = catalogue_.FindStop(stop_name.AsString());
    bus_route.emplace_back(stop_ptr);
    u_stops.emplace(stop_ptr);
  }
  if (!is_roundtrip && !stops.empty())
    bus_route.insert(bus_route.end(),bus_route.rbegin()+1,bus_route.rend());
  
  catalogue_.AddRoute(domain::Bus(std::move(name),
                                  std::move(bus_route),
                                  std::move(u_stops),
                                  is_roundtrip,
                                  first_and_last_stops_are_equal
                                  ));
}

void JSONReader::StopToCatalogue(const json::Node& stop_node) {
  std::string name {stop_node.AsMap().at("name"s).AsString()};
  geo::Coordinates coordinates {
    stop_node.AsMap().at("latitude"s).AsDouble(),
    stop_node.AsMap().at("longitude"s).AsDouble()};
  std::map<std::string,json::Node> road_distances {stop_node.AsMap().at("road_distances"s).AsMap()};

  domain::Stop* stop_ptr = catalogue_.FindStop(name);
  if (stop_ptr == nullptr) {
    domain::Stop stop;
    stop.name = std::move(name);
    stop_ptr = catalogue_.AddStop(std::move(stop));
  }
  stop_ptr->coordinates = std::move(coordinates);
  for (auto& stop_distance_pair : road_distances)
    stop_ptr->neighbour_stops[std::move(stop_distance_pair.first)] = stop_distance_pair.second.AsDouble();
}
                      
svg::Color JSONReader::MakeColor(const json::Node& node) const {
  if (node.IsString())
    return {node.AsString()};
  switch(node.AsArray().size()) {

    case 3 : return svg::Rgb(FitColorValue(node.AsArray().at(0).AsInt()),
                             FitColorValue(node.AsArray().at(1).AsInt()),
                             FitColorValue(node.AsArray().at(2).AsInt()));
    case 4 : return svg::Rgba(FitColorValue(node.AsArray().at(0).AsInt()),
                              FitColorValue(node.AsArray().at(1).AsInt()),
                              FitColorValue(node.AsArray().at(2).AsInt()),
                              node.AsArray().at(3).AsDouble());   
  }
  return "none"s;
}

std::deque<svg::Color> JSONReader::MakePalette(const json::Node& request) const {
  std::deque<svg::Color> color_palette;
  for (const json::Node& node : request.AsArray())
    color_palette.emplace_back(std::move(MakeColor(node)));
  return color_palette;
}

} // json_reader namespace
} // transcat namespace
