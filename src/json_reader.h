//-*-c++-*-
#pragma once

#include <deque>
#include <iostream>
#include <memory>
#include <vector>

#include "domain.h"
#include "json.h"
#include "map_renderer.h"
#include "transport_catalogue.h"

namespace transcat {
namespace input_reader {

class InputReader {
 public:
  explicit InputReader(TransportCatalogue& catalogue);
  virtual ~InputReader() = default;

  virtual map_renderer::MapSettings  GetSettings() const noexcept = 0;
  virtual std::deque<domain::Stats>  GetStats()    const noexcept = 0;

 protected:
  TransportCatalogue& catalogue_;
  std::deque<domain::Stats> stats_ {};

  virtual void ReadBaseRequests()   = 0;
  virtual void ReadRenderSettings() = 0;
  virtual void ReadStatRequests()   = 0;
};


class JSONReader final : public InputReader {
 public:
  JSONReader() = delete;
  JSONReader(TransportCatalogue& catalogue, std::istream& input = std::cin);
 
  map_renderer::MapSettings  GetSettings() const noexcept override;
  std::deque<domain::Stats>  GetStats()    const noexcept override;

 protected:
  void ReadBaseRequests()   override;
  void ReadRenderSettings() override;
  void ReadStatRequests()   override;

 private:
  const json::Document doc_;
  map_renderer::MapSettings settings_;
  
  void RouteToCatalogue(const json::Node& bus_node);
  void StopToCatalogue (const json::Node& stop_node);
  svg::Color MakeColor (const json::Node& node) const;
  std::deque<svg::Color> MakePalette(const json::Node& request) const;

  uint8_t FitColorValue(const int value) const noexcept {
    const int tmp_value = (value < 0) ? 0 : value;
    return (255 - tmp_value < 0) ? 255u : static_cast<uint8_t>(tmp_value);
  }
};

} // input_reader namespace
} // transcat namespace