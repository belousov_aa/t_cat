//-*-c++-*-
#pragma once

#include <iostream>
#include <map>
#include <string>
#include <variant>
#include <vector>

namespace json {

class Node;
using Dict = std::map<std::string, Node>;
using Array = std::vector<Node>;
    
class ParsingError : public std::runtime_error {
 public:
  using runtime_error::runtime_error;
};

class Node final : private std::variant<std::nullptr_t, Array, Dict, bool, int, double, std::string>  {
 public:
  using variant::variant;
  using Value =  variant;
 
  bool IsNull() const noexcept;
  bool IsArray() const noexcept;
  bool IsMap() const noexcept;
  bool IsBool() const noexcept;
  bool IsInt() const noexcept;
  bool IsDouble() const noexcept;
  bool IsPureDouble() const noexcept;
  bool IsString() const noexcept;

  std::nullptr_t AsNull() const;
  const Array& AsArray() const;
  const Dict& AsMap() const;
  bool AsBool() const;
  int AsInt() const;
  double AsDouble() const;
  const std::string& AsString() const;
  
  bool operator==(const Node& rhs) const;
  bool operator!=(const Node& rhs) const;
};


class Document {
 public:
  explicit Document(Node root);

  const Node& GetRoot() const;
  bool operator==(const Document& rhs) const;
  bool operator!=(const Document& rhs) const;
    
 private:
  Node root_;
};


Document Load(std::istream& input);
void Print(const Document& doc, std::ostream& output);

}  // namespace json
