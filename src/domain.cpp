#include "domain.h"

namespace domain {

size_t PtrHasher::operator()(const Stop* pointer) const {
  return std::hash<std::string_view>{}(pointer->name);
}

Bus::Bus(std::string bus_name,
         std::vector<Stop*> bus_route,
         std::unordered_set<Stop*, PtrHasher> u_stops,
         bool is__circle,
         bool first_and_last_stops_are_equal)
    : name(std::move(bus_name))
    , route(std::move(bus_route))
    , unique_stops(std::move(u_stops))
    , is_circle(is__circle)
    , first_and_last_stops_are_equal(first_and_last_stops_are_equal)
    , distance(0)
{
  double tmp_distance = 0;
  for (size_t i=0; i+1 < route.size(); ++i) {
    tmp_distance += ComputeDistance(route[i]->coordinates,
                                    route[i+1]->coordinates);
    if (route[i]->neighbour_stops[route[i+1]->name] != 0)
      distance += route[i]->neighbour_stops[route[i+1]->name];
    else 
      distance += route[i+1]->neighbour_stops[route[i]->name];
  }
  curvature = distance / tmp_distance;
}

} // domain namespace
