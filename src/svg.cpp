#include "svg.h"

namespace svg {

using namespace std::literals;

void Object::Render(const RenderContext& context) const {
  context.RenderIndent();
  RenderObject(context);
  context.out << std::endl;
}

// ---------- Circle ------------------

Circle& Circle::SetCenter(Point center)  {
  center_ = center;
  return *this;
}

Circle& Circle::SetRadius(double radius)  {
  radius_ = radius;
  return *this;
}

void Circle::RenderObject(const RenderContext& context) const {
  auto& out = context.out;
  out << "<circle cx=\""sv << center_.x
      << "\" cy=\""sv << center_.y << "\" "sv
      << "r=\""sv << radius_ << "\""sv;
  RenderAttrs(out);
  out << "/>"sv;
}

// ---------- Polyline ---------------

Polyline& Polyline::AddPoint(Point point)  {
  polyline_.push_back(point);
  return *this;
}

void Polyline::RenderObject(const RenderContext& context) const {
  auto& out = context.out;
  out << "<polyline points=\""sv;
  if (polyline_.size() > 0) {
    out << polyline_.at(0).x << ',' << polyline_.at(0).y ;
    for (size_t i = 1; i < polyline_.size(); ++i)  
      out << ' ' << polyline_.at(i).x << ',' << polyline_.at(i).y ;
  }
  out << '\"';
  RenderAttrs(out);
  out <<"/>"sv;
}

// ----------- Text -----------------

Text& Text::SetPosition(Point pos) {
  pos_ = std::move(pos);
  return *this;
}

Text& Text::SetOffset(Point offset) {
  offset_ = std::move(offset);
  return *this;
}

Text& Text::SetFontSize(uint32_t size) {
  font_size_ = std::move(size);
  return *this;
}

Text& Text::SetFontFamily(std::string font_family) {
  font_family_ = std::move(font_family);
  return *this;
}

Text& Text::SetFontWeight(std::string font_weight) {
  font_weight_ = std::move(font_weight);
  return *this;
}

Text& Text::SetData(std::string data) {
  std::string new_data;
  auto str_ptr = data.begin();
  while (str_ptr != data.end()) {
    if (*str_ptr == '"') {
      new_data.append("&quot;"s);
      ++str_ptr;
      continue;
    }
    if (*str_ptr == '<') {
      new_data.append("&lt;"s);
      ++str_ptr;
      continue;
    }
    if (*str_ptr == '>') {
      new_data.append("&gt;"s);
      ++str_ptr;
      continue;
    }
    if (*str_ptr == '&') {
      new_data.append("&amp;"s);
      ++str_ptr;
      continue;
    }
    if (*str_ptr == '\'') {
      new_data.append("&apos;"s);
      ++str_ptr;
      continue;
    }
    new_data.push_back(*str_ptr);
    ++str_ptr;
  }
  data_ = std::move(new_data);
  return *this;
}

void Text::RenderObject(const RenderContext& context) const {
  auto& out = context.out;
  out << "<text"sv;
  RenderAttrs(out);
  out << " x=\""sv             << pos_.x       << "\""sv
      << " y=\""sv             << pos_.y       << "\""sv
      << " dx=\""sv            << offset_.x    << "\""sv
      << " dy=\""sv            << offset_.y    << "\""sv
      << " font-size=\""sv     << font_size_   << "\""sv;
  if (!font_family_.empty())
    out << " font-family=\""sv << font_family_ << "\""sv;
  if (!font_weight_.empty())
    out << " font-weight=\""sv << font_weight_ << "\""sv;
  out << '>' << data_ << "</text>"sv;
}


void Document::AddPtr(std::unique_ptr<Object>&& obj) {
  ptrs_.emplace_back(std::move(obj));
}

void Document::Render(std::ostream& out) const {
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"sv  << std::endl;
  out << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">"sv << std::endl;
  
  for (size_t i = 0; i < ptrs_.size(); ++i)
    ptrs_.at(i)->Render({out, 2, 2 });
  out << "</svg>"sv;
}

}  // namespace svg
