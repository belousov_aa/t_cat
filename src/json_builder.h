// -*-c++-*-
#pragma once

#include <stack>
#include <string>

#include "json.h"

namespace json {

class Builder;
class ArrayContext;
class DictContext;
class KeyContext;
    
   
class ArrayContext final {
 public:
  explicit ArrayContext(Builder* ptr) : builder_ptr_(ptr) {}
  ArrayContext& Value(Node::Value value);
  ArrayContext& StartArray();
  DictContext StartDict();
  Builder& EndArray();
 private:    
  Builder* builder_ptr_ {nullptr};
};

class DictContext final {
 public:
  explicit DictContext(Builder* ptr) : builder_ptr_(ptr) {}  
  KeyContext Key(std::string key);
  Builder& EndDict();
 private:    
  Builder* builder_ptr_ {nullptr};
};

class KeyContext final {
 public:
  explicit KeyContext(Builder* ptr) : builder_ptr_(ptr) {}    
  DictContext Value(Node::Value value);
  ArrayContext StartArray();
  DictContext StartDict();
 private:    
  Builder* builder_ptr_ {nullptr};
};
    
class Builder final {
 public:
  Node Build() const;
  KeyContext Key(std::string key);
  Builder& Value(Node::Value value);
  ArrayContext StartArray();
  DictContext  StartDict();
  Builder& EndDict();
  Builder& EndArray();
  
 private:
  enum ConType {ARRAY, DICT};
  Node root_;
  bool node_is_complete_ {false};
  bool key_is_expected_  {false};
  std::stack<ConType> nested_containers_;
  std::stack<std::string> keys_;
  std::stack<Dict>  dicts_;
  std::stack<Array> arrays_;
  
  Node GetVariantValue(Node::Value&& value);
};

} // namespace json
