#include "json.h"
#include "json_reader.h"
#include "request_handler.h"
#include "transport_catalogue.h"

int main() {

  transcat::TransportCatalogue db;
  transcat::input_reader::JSONReader reader(db);
  request_handler::RequestHandler handler(db, &reader);  

  handler.PrintStatsAsJSON();

  return 0;
}
