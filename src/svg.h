// -*-c++-*-
#pragma once

#include <cstdint>
#include <iostream>
#include <memory>
#include <optional>
#include <string>
#include <variant>
#include <vector>

namespace svg {

using namespace std::literals;

struct Point {
  Point() = default;
  Point(double x, double y)
      : x(x)
      , y(y)
  {}
  double x = 0;
  double y = 0;
};

struct RenderContext {
  RenderContext(std::ostream& out)
      : out(out) {}

  RenderContext(std::ostream& out, int indent_step, int indent = 0)
      : out(out)
      , indent_step(indent_step)
      , indent(indent) {}

  RenderContext Indented() const {
    return {out, indent_step, indent + indent_step};
  }

  void RenderIndent() const {
    for (int i = 0; i < indent; ++i) {
      out.put(' ');
    }
  }

  std::ostream& out;
  int indent_step = 0;
  int indent = 0;
};

class Object {
 public:
  void Render(const RenderContext& context) const;
  virtual ~Object() = default;

 private:
  virtual void RenderObject(const RenderContext& context) const = 0;
};

enum class StrokeLineCap {
  BUTT,
  ROUND,
  SQUARE,
};

enum class StrokeLineJoin {
  ARCS,
  BEVEL,
  MITER,
  MITER_CLIP,
  ROUND,
};


struct Rgb {
  Rgb() = default;
  Rgb(uint8_t r, uint8_t g, uint8_t b)
      : red(r)
      , green(g)
      , blue(b)
  {}
  uint8_t red = 0;
  uint8_t green = 0;
  uint8_t blue = 0;
};

struct Rgba {
  Rgba() = default;
  Rgba(uint8_t r, uint8_t g, uint8_t b, double o)
      : red(r)
      , green(g)
      , blue(b)
      , opacity(o)
  {
    const double tmp_value = ((opacity - 0.0) < 1e-6)
        ? 0.0
        : opacity;
    opacity = ((1.0 - tmp_value) < 1e-6)
        ? 1.0
        : tmp_value;          
  }
    
  uint8_t red = 0, green = 0, blue = 0; double opacity = 1.0;
};

using Color = std::variant<std::monostate, svg::Rgb, svg::Rgba, std::string>;

inline const Color NoneColor{"none"s};

inline std::ostream& operator<<(std::ostream& out, StrokeLineCap line_cap) {
  using namespace std::literals;
  switch(line_cap) {
    case StrokeLineCap::BUTT   : out << "butt"sv; break;
    case StrokeLineCap::ROUND  : out << "round"sv; break;
    case StrokeLineCap::SQUARE : out << "square"sv; break;
  }
  return out;
}
inline std::ostream& operator<<(std::ostream& out, StrokeLineJoin line_join) {
  using namespace std::literals;
  switch(line_join) {
    case StrokeLineJoin::ARCS       : out << "arcs"sv; break;
    case StrokeLineJoin::BEVEL      : out << "bevel"sv; break;
    case StrokeLineJoin::MITER      : out << "miter"sv; break;
    case StrokeLineJoin::MITER_CLIP : out << "miter-clip"sv; break;
    case StrokeLineJoin::ROUND      : out << "round"sv; break;
  }
  return out;
}

template <typename Owner>
class PathProps {
 public:
  Owner& SetFillColor(Color color) {
    fill_color_ = std::move(color);
    return AsOwner();
  }
  Owner& SetStrokeColor(Color color) {
    stroke_color_ = std::move(color);
    return AsOwner();
  }

  Owner& SetStrokeWidth(double width) {
    stroke_width_ = width;
    return AsOwner();
  }

  Owner& SetStrokeLineCap(StrokeLineCap line_cap) {
    line_cap_ = line_cap;
    return AsOwner();
  }

  Owner& SetStrokeLineJoin(StrokeLineJoin line_join) {
    line_join_ = line_join;
    return AsOwner();
  }

 protected:
  ~PathProps() = default;

  void RenderAttrs(std::ostream& out) const {
    using namespace std::literals;

    if (fill_color_) {
      out << " fill=\""sv;
      PrintColor(out, *fill_color_);
      out << "\""sv;
    }
    if (stroke_color_) {
      out << " stroke=\""sv;
      PrintColor(out, *stroke_color_);
      out << "\""sv;
    }
    if (stroke_width_) {
      out << " stroke-width=\""sv << *stroke_width_ << "\""sv;
    }
    if (line_cap_) {
      out << " stroke-linecap=\""sv << *line_cap_ << "\""sv;
    }
    if (line_join_) {
      out << " stroke-linejoin=\""sv << *line_join_ << "\""sv;
    }
  }

 private:
  std::optional<svg::Color> fill_color_;
  std::optional<svg::Color> stroke_color_;
  std::optional<double> stroke_width_;
  std::optional<StrokeLineCap> line_cap_;
  std::optional<StrokeLineJoin> line_join_;

  Owner& AsOwner() {
    return static_cast<Owner&>(*this);
  }

  std::ostream& PrintColor(std::ostream& out, const Color& color) const {
    if (std::holds_alternative<std::string>(color)) {
      out << std::get<std::string>(color);
      return out;
    }
    if (std::holds_alternative<svg::Rgb>(color)) {
      const auto& tmp = std::get<svg::Rgb>(color);
      out << "rgb("sv 
          << unsigned(tmp.red) << ','
          << unsigned(tmp.green) << ','
          << unsigned(tmp.blue) << ')';
      return out;
    }
    if (std::holds_alternative<svg::Rgba>(color)) {
      const auto& tmp = std::get<svg::Rgba>(color);
      out << "rgba("sv 
          << unsigned(tmp.red) << ','
          << unsigned(tmp.green) << ','
          << unsigned(tmp.blue) << ','
          << tmp.opacity << ')';
      return out;
    }
    out << "none"sv;
    return out;
  }  
};

class Circle final : public Object, public PathProps<Circle> {
 public:
  Circle& SetCenter(Point center);
  Circle& SetRadius(double radius);

 private:
  void RenderObject(const RenderContext& context) const override;
  Point center_;
  double radius_ = 1.0;
};

class Polyline final : public Object, public PathProps<Polyline> {
 public:
  Polyline& AddPoint(Point point);

 private:
  void RenderObject(const RenderContext& context) const override;
  std::vector<Point> polyline_;
};

class Text final : public Object, public PathProps<Text> {
 public:
  Text& SetPosition(Point pos);
  Text& SetOffset(Point offset);
  Text& SetFontSize(uint32_t size);
  Text& SetFontFamily(std::string font_family);
  Text& SetFontWeight(std::string font_weight);
  Text& SetData(std::string data);

 private: 
  void RenderObject(const RenderContext& context) const override;
  Point pos_ {0.0, 0.0};
  Point offset_{0.0, 0.0};
  uint32_t font_size_ {1};
  std::string font_family_;
  std::string font_weight_;
  std::string data_;
};

class ObjectContainer {
 public:
  template <typename Obj>    
  void Add(Obj obj);
  virtual void AddPtr(std::unique_ptr<Object>&& obj) = 0;
};

class Drawable {
 public:
  virtual void Draw(ObjectContainer&) const = 0;
  virtual ~Drawable() = default;
};

class Document final : public ObjectContainer {
 public:
  void AddPtr(std::unique_ptr<Object>&& obj) override;
  void Render(std::ostream& out) const;

 private:
  std::vector<std::unique_ptr<Object>> ptrs_;
  std::unique_ptr<Object> ptr_;
};

template <typename Obj>    
void ObjectContainer::Add(Obj obj) {
  AddPtr(std::make_unique<Obj>(std::move(obj)));
}
    
}  // namespace svg
