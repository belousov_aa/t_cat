// -*-c++-*-
#pragma once

#include <deque>
#include <iostream>

#include "domain.h"
#include "map_renderer.h"
#include "json.h"
#include "json_reader.h"
#include "svg.h"
#include "transport_catalogue.h"

namespace request_handler {

using namespace transcat;

class DataForRenderer {
 public:
  DataForRenderer() = delete;
  explicit DataForRenderer(const transcat::TransportCatalogue& catalogue,
                           const map_renderer::MapSettings& settings);
  
  std::deque<map_renderer::BusDataToRender> ExtractRenderableData();
  std::map<std::string_view, svg::Point, std::less<>> ExtractActiveStops();
  map_renderer::MapSettings ExtractSettings();
  
 private:  
  std::deque<map_renderer::BusDataToRender> renderable_data_;
  std::map<std::string_view, svg::Point, std::less<>> active_stops_;
  const transcat::TransportCatalogue& catalogue_;
  map_renderer::MapSettings settings_;
};


class RequestHandler {
 public:
  friend class JSONPrinter;
  RequestHandler(TransportCatalogue& db, input_reader::InputReader* reader);

  void PrintStatsAsJSON() const;

 private:
  const TransportCatalogue& db_;
  const map_renderer::MapSettings settings_;
  const std::deque<domain::Stats> stat_requests_;

  svg::Color ColorFromJSON(const json::Node& node) const;
};

class Printer {
 public:
  Printer(const RequestHandler& handler) : handler_(handler) {}
  virtual ~Printer() = default;

  virtual void Print(std::ostream& out = std::cout) const = 0;
 protected:
  const RequestHandler& handler_;
};

class JSONPrinter final : public Printer {
 public:
  JSONPrinter(const RequestHandler& handler) : Printer(handler) {}
  void Print(std::ostream& out = std::cout) const override;

 private:
  json::Node PrintBus(const Stats& stat)  const;
  json::Node PrintStop(const Stats& stat) const;
  json::Node PrintMap(const Stats& stat)  const; 
};

    
inline const double EPSILON = 1e-6;
inline bool IsZero(double value) {
  return std::abs(value) < EPSILON;
}

class SphereProjector {
 public:
  template <typename PointInputIt>
  SphereProjector(PointInputIt points_begin,
                  PointInputIt points_end, double max_width,
                  double max_height, double padding)
      : padding_(padding) {
    if (points_begin == points_end) {
      return;
    }

    const auto [left_it, right_it]
        = std::minmax_element(points_begin, points_end, [](auto lhs, auto rhs) {
          return lhs.lng < rhs.lng;
        });
    min_lon_ = left_it->lng;
    const double max_lon = right_it->lng;

    const auto [bottom_it, top_it]
        = std::minmax_element(points_begin, points_end, [](auto lhs, auto rhs) {
          return lhs.lat < rhs.lat;
        });
    const double min_lat = bottom_it->lat;
    max_lat_ = top_it->lat;

    std::optional<double> width_zoom;
    if (!IsZero(max_lon - min_lon_)) {
      width_zoom = (max_width - 2 * padding) / (max_lon - min_lon_);
    }

    std::optional<double> height_zoom;
    if (!IsZero(max_lat_ - min_lat)) {
      height_zoom = (max_height - 2 * padding) / (max_lat_ - min_lat);
    }

    if (width_zoom && height_zoom) {
      zoom_coeff_ = std::min(*width_zoom, *height_zoom);
    } else if (width_zoom) {
      zoom_coeff_ = *width_zoom;
    } else if (height_zoom) {
      zoom_coeff_ = *height_zoom;
    }
  }

  svg::Point operator()(geo::Coordinates coords) const {
    return {(coords.lng - min_lon_) * zoom_coeff_ + padding_,
      (max_lat_ - coords.lat) * zoom_coeff_ + padding_};
  }

 private:
  double padding_;
  double min_lon_ = 0;
  double max_lat_ = 0;
  double zoom_coeff_ = 0;
};
    
} // request_handler namespace
