#include "transport_catalogue.h"

#include <algorithm>

namespace transcat {

Bus* TransportCatalogue::AddRoute(Bus&& bus) {
  Bus* bus_ptr = &all_routes_.emplace_back(std::move(bus));
  for (const Stop* const stop_ptr : bus_ptr->unique_stops)
    stop_to_buses_[stop_ptr].insert(bus_ptr->name);
  bus_names_.insert(bus_ptr->name);
  return bus_ptr;
}

Bus* TransportCatalogue::AddRoute(const Bus& bus) {
  return AddRoute(Bus(bus));
}

Stop* TransportCatalogue::AddStop(Stop&& stop) {
  return &all_stops_.emplace_back(std::move(stop));
}

Stop* TransportCatalogue::AddStop(const Stop& stop) {
  return AddStop(Stop(stop));
}

Bus* TransportCatalogue::FindRoute(std::string_view bus_name) {
  auto result = std::find_if(all_routes_.begin(), all_routes_.end(),
                             [bus_name](const Bus& bus){
                               return bus_name == bus.name;
                             });
  return (result == all_routes_.end()) ? nullptr : &(*result);
}

Stop* TransportCatalogue::FindStop(std::string_view stop_name) {
  auto result = std::find_if(all_stops_.begin(), all_stops_.end(),
                             [stop_name](const Stop& stop){
                               return stop_name == stop.name;
                             });
  return (result == all_stops_.end()) ? nullptr : &(*result);
}

const Bus* TransportCatalogue::FindRoute(std::string_view bus_name) const {
  auto result = std::find_if(all_routes_.begin(), all_routes_.end(),
                             [bus_name](const Bus& bus){
                               return bus_name == bus.name;
                             });
  return (result == all_routes_.end()) ? nullptr : &(*result);
}

const Stop* TransportCatalogue::FindStop(std::string_view stop_name) const {
  auto result = std::find_if(all_stops_.begin(), all_stops_.end(),
                             [stop_name](const Stop& stop){
                               return stop_name == stop.name;
                             });
  return (result == all_stops_.end()) ? nullptr : &(*result);
}

std::set<std::string_view, std::less<>>
TransportCatalogue::GetBusNames() const noexcept {
  return bus_names_;
}

std::optional<RouteInfo> TransportCatalogue::GetRouteInfo(std::string_view bus_name) const {
  const Bus* bus_ptr = FindRoute(bus_name);
  return  (bus_ptr == nullptr)
      ? std::nullopt
      : std::optional<RouteInfo>
      {
        {bus_name,
         bus_ptr->route.size(),
         bus_ptr->unique_stops.size(),
         bus_ptr->distance,
         bus_ptr->curvature}
      };  
}

const std::set<std::string_view, std::less<>>* 
TransportCatalogue::GetStopInfo(std::string_view stop) const {
  return (stop_to_buses_.find(FindStop(stop)) != stop_to_buses_.end())
      ? &stop_to_buses_.at(FindStop(stop))
      : nullptr;
}

std::set<std::string_view, std::less<>>* 
TransportCatalogue::GetStopInfo(std::string_view stop) {
  return &stop_to_buses_[FindStop(stop)];
}

} // transcat namespace
