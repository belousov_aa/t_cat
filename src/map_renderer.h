//-*-c++-*-
#pragma once

#include <deque>
#include <map>
#include <vector>

#include "svg.h"

namespace map_renderer {
using namespace std::literals;

class MapSettings {
 public:
  MapSettings() = default;
  MapSettings(const double width,
              const double height,
              const double padding,
              const double line_width,
              const double stop_radius,
              const int bus_label_font_size,
              const int stop_label_font_size,
              const double underlayer_width,
              const svg::Point bus_label_offset,
              const svg::Point stop_label_offset,
              svg::Color&& underlayer_color,
              std::deque<svg::Color>&& color_palette);
  
  void SetWidth(const double value);
  void SetHeight(const double value); 
  void SetPadding(const double value);
  void SetLineWidth(const double value);
  void SetStopRadius(const double value);
  void SetBusLabelFontSize(const int value);
  void SetStopLabelFontSize(const int value);
  void SetUnderlayerWidth(const double value);
  void SetBusLabelOffset(const svg::Point& value);
  void SetStopLabelOffset(const svg::Point& value);
  void SetUnderlayerColor(svg::Color&& value);
  void SetColorPalette(std::deque<svg::Color>&& value);
  
  double GetWidth() const noexcept;
  double GetHeight() const noexcept;
  double GetPadding() const noexcept;
  double GetLineWidth() const noexcept;
  double GetStopRadius() const noexcept;
  int GetBusLabelFontSize() const noexcept;
  int GetStopLabelFontSize() const noexcept;
  double GetUnderlayerWidth() const noexcept;
  svg::Point GetBusLabelOffset() const noexcept;
  svg::Point GetStopLabelOffset() const noexcept;
  svg::Color GetUnderlayerColor() const noexcept;
  std::deque<svg::Color> GetColorPalette() const noexcept;
  
 private:
  double width_                {0.0};
  double height_               {0.0};
  double padding_              {0.0};
  double line_width_           {0.0};
  double stop_radius_          {1.0};
  int bus_label_font_size_     {1};
  int stop_label_font_size_    {1};
  double underlayer_width_     {0.0};
  svg::Point bus_label_offset_;
  svg::Point stop_label_offset_;
  svg::Color underlayer_color_           {"none"s};
  std::deque<svg::Color> color_palette_ {{"none"s}};

  template <typename Type>
  Type FitValue(const Type value,
                const Type range_min,
                const Type range_max) const noexcept {
    const Type tmp_value = ((value - range_min) < 1e-6)
        ? range_min
        : value;
    return ((range_max - tmp_value) < 1e-6)
        ? range_max
        : tmp_value;
  }
};

struct BusDataToRender {
  std::string_view name;
  std::deque<std::pair<std::string_view,svg::Point>> route_points;
  svg::Color stroke_color;
  bool is_circle;
  bool first_and_last_stops_are_equal;
  constexpr static const svg::Color& fill_color = svg::NoneColor;
  double stroke_width{0};
  constexpr static const svg::StrokeLineCap stroke_linecap{svg::StrokeLineCap::ROUND};
  constexpr static const svg::StrokeLineJoin stroke_linejoin{svg::StrokeLineJoin::ROUND};
};

class MapRenderer final : public svg::Drawable {
 public:
  MapRenderer() = delete;
  explicit MapRenderer(MapSettings&& settings,
                       std::deque<BusDataToRender>&& renderable_data,
                       std::map<std::string_view,
                       svg::Point, std::less<>>&& active_stops);
  
  void Draw(svg::ObjectContainer& container) const override;
  
 private:
  const MapSettings settings_;
  std::deque<BusDataToRender> renderable_data_;
  std::map<std::string_view, svg::Point, std::less<>> active_stops_;
  
  void DrawRoutes(svg::ObjectContainer& container) const;
  void DrawRouteNames(svg::ObjectContainer& container) const;
  void DrawBusName(svg::ObjectContainer& container,
                   const std::string& bus_name,
                   const svg::Color& color,
                   const svg::Point& point) const;
  void DrawStopDots(svg::ObjectContainer& container) const;
  void DrawStopNames(svg::ObjectContainer& container) const;
};
  
} // map_renderer namespace
