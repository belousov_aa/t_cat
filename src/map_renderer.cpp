#include "map_renderer.h"

#include <algorithm> 

namespace map_renderer {

MapSettings::MapSettings(const double width,
                         const double height,
                         const double padding,
                         const double line_width,
                         const double stop_radius,
                         const int bus_label_font_size,
                         const int stop_label_font_size,
                         const double underlayer_width,
                         const svg::Point bus_label_offset,
                         const svg::Point stop_label_offset,
                         svg::Color&& underlayer_color,
                         std::deque<svg::Color>&& color_palette) 
{
  SetWidth(width);
  SetHeight(height);
  SetPadding(padding);
  SetLineWidth(line_width);
  SetStopRadius(stop_radius);
  SetBusLabelFontSize(bus_label_font_size);
  SetStopLabelFontSize(stop_label_font_size);
  SetUnderlayerWidth(underlayer_width);
  SetBusLabelOffset(bus_label_offset);
  SetStopLabelOffset(stop_label_offset);
  SetUnderlayerColor(std::move(underlayer_color));
  SetColorPalette(std::move(color_palette));
}

void MapSettings::SetWidth(const double value) {
  width_ = FitValue(value, 0.0, 100000.0);
}

void MapSettings::SetHeight(const double value) {
  height_ = FitValue(value, 0.0, 100000.0);
}
 
void MapSettings::SetPadding(const double value) {
  padding_ = FitValue(value, 0.0, std::min(width_, height_)/2);
}

void MapSettings::SetLineWidth(const double value) {
  line_width_ = FitValue(value, 0.0, 100000.0);
}

void MapSettings::SetStopRadius(const double value) {
  stop_radius_ = FitValue(value, 0.0, 100000.0);
}

void MapSettings::SetBusLabelFontSize(const int value) {
  bus_label_font_size_ = FitValue(value, 0, 100000);
}

void MapSettings::SetStopLabelFontSize(const int value) {
  stop_label_font_size_ = FitValue(value, 0, 100000);
}

void MapSettings::SetUnderlayerWidth(const double value) {
  underlayer_width_ = FitValue(value, 0.0, 100000.0);
}

void MapSettings::SetBusLabelOffset(const svg::Point& value) {
  bus_label_offset_.x = FitValue(value.x, -100000.0, 100000.0);
  bus_label_offset_.y = FitValue(value.y, -100000.0, 100000.0);
}

void MapSettings::SetStopLabelOffset(const svg::Point& value) {
  stop_label_offset_.x = FitValue(value.x, -100000.0, 100000.0);
  stop_label_offset_.y = FitValue(value.y, -100000.0, 100000.0);
}

void MapSettings::SetUnderlayerColor(svg::Color&& value) {
  underlayer_color_ = std::move(value);
}

void MapSettings::SetColorPalette(std::deque<svg::Color>&& value) {
  color_palette_.clear();
  if (value.empty())
    color_palette_.emplace_back("none"s);
  for (const svg::Color& color : value)
    color_palette_.emplace_back(color);
}

double MapSettings::GetWidth() const noexcept {
  return width_;
}

double MapSettings::GetHeight() const noexcept {
  return height_;
}

double MapSettings::GetPadding() const noexcept {
  return padding_;
}

double MapSettings::GetLineWidth() const noexcept {
  return line_width_;
}

double MapSettings::GetStopRadius() const noexcept {
  return stop_radius_;
}

int MapSettings::GetBusLabelFontSize() const noexcept {
  return bus_label_font_size_;
}

int MapSettings::GetStopLabelFontSize() const noexcept {
  return stop_label_font_size_;
}

double MapSettings::GetUnderlayerWidth() const noexcept {
  return underlayer_width_;
}

svg::Point MapSettings::GetBusLabelOffset() const noexcept {
  return bus_label_offset_;
}

svg::Point MapSettings::GetStopLabelOffset() const noexcept {
  return stop_label_offset_;
}

svg::Color MapSettings::GetUnderlayerColor() const noexcept {
  return underlayer_color_;
}

std::deque<svg::Color> MapSettings::GetColorPalette() const noexcept {
  return color_palette_;
}


MapRenderer::MapRenderer(MapSettings&& settings,
                         std::deque<BusDataToRender>&& renderable_data,
                         std::map<std::string_view,
                         svg::Point, std::less<>>&& active_stops)
    : settings_(std::move(settings))
    , renderable_data_(std::move(renderable_data))
    , active_stops_(std::move(active_stops))
{}


void MapRenderer::Draw(svg::ObjectContainer& container) const {
  DrawRoutes(container);
  DrawRouteNames(container);
  DrawStopDots(container);
  DrawStopNames(container);
}

void MapRenderer::DrawRoutes(svg::ObjectContainer& container) const {
  for (const BusDataToRender& bus_data : renderable_data_) {
    auto route = svg::Polyline();
    for (const auto& name_point_pair : bus_data.route_points)
      route.AddPoint(name_point_pair.second);   
    route.SetFillColor(bus_data.fill_color).
        SetStrokeColor(bus_data.stroke_color).
        SetStrokeWidth(bus_data.stroke_width).
        SetStrokeLineCap(bus_data.stroke_linecap).
        SetStrokeLineJoin(bus_data.stroke_linejoin);
    container.Add(std::move(route));
  }
}

void MapRenderer::DrawRouteNames(svg::ObjectContainer& container) const {
  for (const BusDataToRender& bus_data : renderable_data_) {
    if (bus_data.route_points.empty())
      continue;
      
    DrawBusName(container, std::string(bus_data.name),
                bus_data.stroke_color,
                bus_data.route_points.front().second);
    
    if (!bus_data.is_circle && !bus_data.first_and_last_stops_are_equal)
      DrawBusName(container, std::string(bus_data.name),
                  bus_data.stroke_color,
                  bus_data.route_points.at(
                      (bus_data.route_points.size() - 1) / 2).second);
  }
}

void MapRenderer::DrawBusName(svg::ObjectContainer& container,
                              const std::string& bus_name,
                              const svg::Color& color,
                              const svg::Point& point) const {
  using namespace std::literals;
  auto underlayer = svg::Text().
      SetFillColor(settings_.GetUnderlayerColor()).
      SetStrokeColor(settings_.GetUnderlayerColor()).
      SetStrokeWidth(settings_.GetUnderlayerWidth()).
      SetStrokeLineCap(svg::StrokeLineCap::ROUND).
      SetStrokeLineJoin(svg::StrokeLineJoin::ROUND). 
      SetPosition(point).
      SetOffset(settings_.GetBusLabelOffset()).
      SetFontSize(settings_.GetBusLabelFontSize()).
      SetFontFamily("Verdana"s).
      SetFontWeight("bold"s).
      SetData(bus_name);
  
  auto text = svg::Text().
      SetFillColor(color).
      SetPosition(point).
      SetOffset(settings_.GetBusLabelOffset()).
      SetFontSize(settings_.GetBusLabelFontSize()).
      SetFontFamily("Verdana"s).
      SetFontWeight("bold"s).
      SetData(bus_name);
  
  container.Add(std::move(underlayer));
  container.Add(std::move(text));
}

void MapRenderer::DrawStopDots(svg::ObjectContainer& container) const {
  using namespace std::literals;
  for (const auto& name_point_pair : active_stops_) {
    auto dot = svg::Circle().
        SetCenter(name_point_pair.second).
        SetRadius(settings_.GetStopRadius()).
        SetFillColor("white"s);
    container.Add(std::move(dot));
  }
}

void MapRenderer::DrawStopNames(svg::ObjectContainer& container) const {
  using namespace std::literals;
  for (const auto& name_point_pair : active_stops_) {
    auto underlayer = svg::Text().
        SetFillColor(settings_.GetUnderlayerColor()).
        SetStrokeColor(settings_.GetUnderlayerColor()).
        SetStrokeWidth(settings_.GetUnderlayerWidth()).
        SetStrokeLineCap(svg::StrokeLineCap::ROUND).
        SetStrokeLineJoin(svg::StrokeLineJoin::ROUND).
        SetPosition(name_point_pair.second).
        SetOffset(settings_.GetStopLabelOffset()).
        SetFontSize(settings_.GetStopLabelFontSize()).
        SetFontFamily("Verdana"s).
        SetData(std::string(name_point_pair.first));
  
    auto text = svg::Text().
        SetFillColor("black"s).
        SetPosition(name_point_pair.second).
        SetOffset(settings_.GetStopLabelOffset()).
        SetFontSize(settings_.GetStopLabelFontSize()).
        SetFontFamily("Verdana"s).
        SetData(std::string(name_point_pair.first));
  
    container.Add(std::move(underlayer));
    container.Add(std::move(text));
  }
}

} // map_renderer namespace
