//-*-c++-*-
#pragma once

#include <deque>
#include <set>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "domain.h"
#include "geo.h"

namespace transcat {
using namespace geo;
using namespace domain;

struct RouteInfo {
  std::string_view bus_name;
  size_t stops_on_route {0};
  size_t unique_stops   {0};
  double distance       {0};
  double curvature      {0};
};

class TransportCatalogue final { 
 public:
  Bus*  AddRoute(Bus&& bus);
  Bus*  AddRoute(const Bus& bus);
  Stop* AddStop(Stop&& stop);
  Stop* AddStop(const Stop& stop);
  Bus*  FindRoute(std::string_view bus_name);
  Stop* FindStop(std::string_view stop_name);
  const Bus*  FindRoute(std::string_view bus_name) const;
  const Stop* FindStop(std::string_view stop_name) const;

  std::set<std::string_view, std::less<>> GetBusNames() const noexcept;
  std::optional<RouteInfo> GetRouteInfo(std::string_view bus) const;
  const std::set<std::string_view, std::less<>>*
  GetStopInfo(std::string_view stop) const;
    
  std::set<std::string_view, std::less<>>* 
  GetStopInfo(std::string_view stop);

 private:    
  std::deque<Bus>  all_routes_;
  std::deque<Stop> all_stops_;
  std::unordered_map<const Stop*, std::set<std::string_view, std::less<>>,
                     PtrHasher> stop_to_buses_;
  std::set<std::string_view, std::less<>> bus_names_;
};

} // transcat namespace
