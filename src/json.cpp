#include "json.h"

#include <initializer_list>

namespace json {

namespace {

using namespace std::literals;

Node LoadNode(std::istream& input);
Node LoadArray(std::istream& input);
Node LoadDict(std::istream& input);
Node LoadNumber(std::istream& input);
Node LoadString(std::istream& input);

Node LoadArray(std::istream& input) {
  Array result;
  char c;

  while (input >> c) {
    switch(c) {
      case ']' : return Node(std::move(result));
      case ',' : break;
      default  : {
        input.putback(c);
        result.emplace_back(std::move(LoadNode(input)));
      }
    }
  }
  throw ParsingError(" \"]\" is expected"s);
  return {};
}

Node LoadDict(std::istream& input) {
  Dict result;
  char c;
  std::string key;

  while (input >> c) {
    switch(c) {
      case '}'  : return Node(std::move(result));
      case '"'  : key = LoadString(input).AsString(); break;
      case ':'  : result.insert({std::move(key), std::move(LoadNode(input))});
    }
  }
  throw ParsingError(" \"}\" is expected"s);
  return {};
}
 
Node LoadNumber(std::istream& input) {
  
  using namespace std::literals;

  std::string parsed_num;

  auto read_char = [&parsed_num, &input] {
    parsed_num += static_cast<char>(input.get());
    if (!input) {
      throw ParsingError("Failed to read number from stream"s);
    }
  };

  auto read_digits = [&input, read_char] {
    if (!std::isdigit(input.peek())) {
      throw ParsingError("A digit is expected"s);
    }
    while (std::isdigit(input.peek())) {
      read_char();
    }
  };

  switch (input.peek()) 
    case '-' : read_char();

  switch (input.peek()) {
    case '0' : read_char(); break;
    default  : read_digits();
  }

  bool is_int = true;

  switch (input.peek()) 
    case '.' : {
      read_char();
      read_digits();
      is_int = false;
      break;
    }
  
  switch (input.peek()) 
    case 'e' : case 'E' : {
      read_char();
      switch (input.peek())
        case '+' : case '-' : read_char();
      read_digits();
      is_int = false;
    }

  try {
    if (is_int) {
      try {
        return Node(std::stoi(parsed_num));
      } catch (...) {}
    }
    return Node(std::stod(parsed_num));
  } catch (...) {
    throw ParsingError("Failed to convert "s + parsed_num + " to number"s);
  }
}

Node LoadString(std::istream& input) {
  std::string line;
  char letter;
  
  while ((letter = input.get()) && !input.eof()) {

    switch (letter) {
      case '"'  : return Node(std::move(line));
      case '\\' : {
        switch (input.get()) {
          case '\\' : line.push_back('\\'); break;
          case 't'  : line.push_back('\t'); break;
          case 'n'  : line.push_back('\n'); break;
          case 'r'  : line.push_back('\r'); break;
          case '"'  : line.push_back('\"'); 
        }
        break;
      }
      default   : line.push_back(letter);
    }
  }
  throw ParsingError("Missing apostrophe"s);
  return {};
}

Node LoadNode(std::istream& input) {

  char first_letter;
  input >> first_letter;
  switch (first_letter) {
    case '['  : return LoadArray(input);
    case '{'  : return LoadDict(input);
    case '"'  : return LoadString(input);
  }

  input.putback(first_letter);
  std::string word;

  switch (first_letter) {
    case 'f' : {
      for (uint8_t i = 0; i < 5; ++i)
        word.push_back(input.get());
      if (word == "false"s)
        return Node(false);
      throw ParsingError("Wrong input for false"s);
    }
    case 'n' : {
      for (uint8_t i = 0; i < 4; ++i)
        word.push_back(input.get());
      if (word == "null"s)
        return Node(nullptr);
      throw ParsingError("Wrong input for null"s);
    }
    case 't' : {
      for (uint8_t i = 0; i < 4; ++i)
        word.push_back(input.get());
      if (word == "true"s)
        return Node(true);
      throw ParsingError("Wrong input for true"s);
    }
  }

  return LoadNumber(input);

}

}  // namespace

bool Node::IsNull() const noexcept {
  return std::holds_alternative<std::nullptr_t>(*this);
}

bool Node::IsArray() const noexcept {
  return std::holds_alternative<Array>(*this);
}

bool Node::IsMap() const noexcept {
  return std::holds_alternative<Dict>(*this);
}

bool Node::IsBool() const noexcept {
  return std::holds_alternative<bool>(*this);
}

bool Node::IsInt() const noexcept {
  return std::holds_alternative<int>(*this);
}

bool Node::IsDouble() const noexcept {
  return std::holds_alternative<double>(*this) ||
      std::holds_alternative<int>(*this);
}

bool Node::IsPureDouble() const noexcept {
  return (std::holds_alternative<double>(*this));
}

bool Node::IsString() const noexcept {
  return std::holds_alternative<std::string>(*this);
}


std::nullptr_t Node::AsNull() const {
  if (!IsNull())
    throw std::logic_error("Node не является Null"s);
  return nullptr;
}

const Array& Node::AsArray() const {
  if (!IsArray())
    throw std::logic_error("Node не является Array"s);
  return std::get<Array>(*this);
}

const Dict& Node::AsMap() const {
  if (!IsMap())
    throw std::logic_error("Node не является Map"s);
  return std::get<Dict>(*this);
}

bool Node::AsBool() const {
  if (!IsBool())
    throw std::logic_error("Node не является Bool"s);
  return std::get<bool>(*this);
}

int Node::AsInt() const {
  if (!IsInt())
    throw std::logic_error("Node не является Integer"s);
  return std::get<int>(*this);
}

double Node::AsDouble() const {
  if (!IsDouble())
    throw std::logic_error("Node не является Double"s);
  return (std::holds_alternative<int>(*this))
      ? static_cast<double>(std::get<int>(*this))
      : std::get<double>(*this);
}

const std::string& Node::AsString() const {
  if (!IsString())
    throw std::logic_error("Node не является String"s);
  return std::get<std::string>(*this);
}

bool Node::operator==(const Node& rhs) const {
  return *dynamic_cast<const Value*>(this) == *dynamic_cast<const Value*>(&rhs);
}

bool Node::operator!=(const Node& rhs) const {
  return *dynamic_cast<const Value*>(this) != *dynamic_cast<const Value*>(&rhs);
}

Document::Document(Node root)
    : root_(std::move(root))
{}

const Node& Document::GetRoot() const {
  return root_;
}

bool Document::operator==(const Document& rhs) const {
  return root_ == rhs.root_;
}
bool Document::operator!=(const Document& rhs) const {
  return root_ != rhs.root_;
}

Document Load(std::istream& input) {
  return Document(LoadNode(input));
}

void Print(const Document& doc, std::ostream& output) {
  using namespace std::literals;
  
  const Node& element = doc.GetRoot();

  if (element.IsNull()) {
    output << "null"sv;
    return;
  }
  
  if (element.IsArray()) {
    const Array& array = element.AsArray();
    output << "[\n"sv;
    
    if (array.empty()) {
      output << "]\n"sv;
      return;
    }   
    
    Print(Document(array.at(0)), output);
    
    for (size_t i = 1; i < array.size(); ++i) {
      output << ",\n "sv;
      Print(Document(array.at(i)), output);
    }
    output << "]"sv;
    return;
  }
  
  if (element.IsMap()) {
    output << "{\n"sv;
    const auto& dict = element.AsMap();
    
    if (dict.empty()) {
      output << "}\n"sv;
      return;
    }
    
    output << '\"' << dict.begin()->first << '\"';
    output << ": "sv;
    Print(Document{dict.begin()->second}, output);

    for (auto it = ++dict.begin() ; it != dict.end(); ++it) {
      output << ",\n "sv
             << '\"' << it->first << '\"';
      output << ": "sv;
      Print(Document{it->second}, output);
    }
    output << "}\n"sv;
    return;
  }

  if (element.IsBool()) {
    output << std::boolalpha << element.AsBool();
    return;
  }

  if (element.IsPureDouble()) {
    output << element.AsDouble();
    return;
  }  
    
  if (element.IsInt()) {
    output << element.AsInt();
    return;
  }

  if (element.IsString()) {
    output << '\"';
    for (const char letter : element.AsString()) {
      switch(letter) {
        case '\n' : output << '\\' << 'n'; break;
        case '\r' : output << '\\' << 'r'; break;
        case '\t' : output << '\\' << 't'; break;
        case '\\' : output << "\\\\"sv   ; break;
        case '"'  : output << '\\' << '"'; break;         
        default   : output << letter;
      }
    }  
    output << '\"';
  }
}

}  // namespace json
