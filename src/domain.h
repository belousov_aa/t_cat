//-*-c++-*-
#pragma once

#include <cstddef>
#include <functional>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "geo.h"

namespace domain {

struct Stop {
  std::string name;
  geo::Coordinates coordinates;
  std::unordered_map<std::string, double,
                     std::hash<std::string>>
  neighbour_stops;
};

struct PtrHasher {
  size_t operator()(const Stop* pointer) const;
};

struct Bus {
  Bus() = delete;
  explicit Bus(std::string bus_name,
               std::vector<Stop*> bus_route,
               std::unordered_set<Stop*, PtrHasher> u_stops,
               bool is__circle,
               bool first_and_last_stops_are_equal
               );
  std::string name;
  std::vector<Stop*> route;
  std::unordered_set<Stop*, PtrHasher> unique_stops;
  bool is_circle;
  bool first_and_last_stops_are_equal;
  double distance;
  double curvature;
};

struct Stats {
  enum Type {Stop, Bus, Map};
  int id;
  Type type;
  std::string name;
};

} //domain namespace
