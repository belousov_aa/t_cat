#include "request_handler.h"

#include <iomanip>
#include <utility>

#include "json.h"
#include "json_builder.h"
#include "json_reader.h"

namespace request_handler {

RequestHandler::RequestHandler(TransportCatalogue& db,
                               input_reader::InputReader* reader)
    : db_(db)
    , settings_(reader->GetSettings())
    , stat_requests_(reader->GetStats())
{}

void RequestHandler::PrintStatsAsJSON() const {
  JSONPrinter printer(*this);
  printer.Print();
}

void JSONPrinter::Print(std::ostream& out) const {
  json::Array output_array;
  for (const Stats& stat : handler_.stat_requests_) {
    switch (stat.type) {
      case Stats::Type::Bus  : output_array.emplace_back(PrintBus(stat));  break;
      case Stats::Type::Stop : output_array.emplace_back(PrintStop(stat)); break;
      case Stats::Type::Map  : output_array.emplace_back(PrintMap(stat));  break;
    }
  }
  json::Print(json::Document(json::Node(std::move(output_array))), out);
}

json::Node JSONPrinter::PrintBus(const Stats& stat) const {
  using namespace std::literals;
  std::optional<RouteInfo> info = handler_.db_.GetRouteInfo(std::string_view(stat.name));
  if (info == std::nullopt)
    return {json::Builder{}.
      StartDict().
      Key("request_id"s).Value(stat.id).
      Key("error_message"s).Value("not found"s).
      EndDict().
      Build()};

  return {json::Builder{}.
    StartDict().
    Key("request_id"s).Value(stat.id).
    Key("stop_count"s).Value(static_cast<int>(info.value().stops_on_route)).
    Key("unique_stop_count"s).Value(static_cast<int>(info.value().unique_stops)).
    Key("route_length"s).Value(info.value().distance).
    Key("curvature"s).Value(info.value().curvature).
    EndDict().
    Build()};
}

json::Node JSONPrinter::PrintStop(const Stats& stat) const {
  using namespace std::literals;

  if (handler_.db_.FindStop(stat.name) == nullptr )
    return {json::Builder{}.
      StartDict().
      Key("request_id"s).Value(stat.id).
      Key("error_message"s).Value("not found"s).
      EndDict().
      Build()};
    
  const auto* const set_of_buses_ptr = handler_.db_.GetStopInfo(stat.name);
  json::Array array;
  if (set_of_buses_ptr != nullptr) {     
    for (std::string_view str : *set_of_buses_ptr)
      array.emplace_back(json::Node(std::string(str)));
  }
  return {json::Builder{}.
    StartDict().
    Key("request_id"s).Value(stat.id).
    Key("buses"s).Value(std::move(array)).
    EndDict().
    Build()};
}
    
json::Node JSONPrinter::PrintMap(const Stats& stat) const {
  using namespace std::literals;
  
  svg::Document doc;
  DataForRenderer data(handler_.db_, handler_.settings_);
  map_renderer::MapRenderer map_renderer(std::move(data.ExtractSettings()),
                                         std::move(data.ExtractRenderableData()),
                                         std::move(data.ExtractActiveStops()));
  map_renderer.Draw(doc);     
  std::stringstream out;
  doc.Render(out);
  return {json::Builder{}.
    StartDict().
    Key("request_id"s).Value(stat.id).
    Key("map"s).Value(std::move(out.str())).
    EndDict().
    Build()};
}

DataForRenderer::DataForRenderer(const transcat::TransportCatalogue& catalogue,
                                 const map_renderer::MapSettings& settings)
    : catalogue_(catalogue)
    , settings_(settings)
{
  std::vector<geo::Coordinates> all_active_coords;
  
  for (std::string_view bus_name : catalogue_.GetBusNames()) {
    const auto& stops = catalogue_.FindRoute(bus_name)->unique_stops;
    for (const domain::Stop* stop_ptr : stops)
      all_active_coords.emplace_back(stop_ptr->coordinates);
  }
 
  std::sort(all_active_coords.begin(), all_active_coords.end(),
            [&](const auto& rhs, const auto& lhs) {
              return ((lhs.lat - rhs.lat) < 0) && ((lhs.lng - rhs.lng) < 0);
            });
 
  all_active_coords.erase(std::unique(all_active_coords.begin(),
                                      all_active_coords.end(),
                                      [&](const auto& rhs, const auto& lhs) {
                                        return (std::abs(lhs.lat - rhs.lat) < 1e-6) &&
                                            (std::abs(lhs.lng - rhs.lng) < 1e-6);}),
                          all_active_coords.end());
  
  SphereProjector projector(all_active_coords.begin(), 
                            all_active_coords.end(),
                            settings_.GetWidth(),
                            settings_.GetHeight(),
                            settings_.GetPadding());    
  size_t color_counter {0};
  const size_t mod {settings_.GetColorPalette().size()};
  
  for (std::string_view bus_name : catalogue_.GetBusNames()) {  
    map_renderer::BusDataToRender bus_data;

    const Bus* bus_ptr = catalogue_.FindRoute(bus_name);
    const auto& stops = bus_ptr->route;

    bus_data.name = bus_name;
    bus_data.stroke_width = settings_.GetLineWidth();
    bus_data.stroke_color = (settings_.GetColorPalette().size() > 0)
        ? settings_.GetColorPalette().at(color_counter++ % mod)
        : svg::NoneColor;
    bus_data.is_circle = bus_ptr->is_circle;
    bus_data.first_and_last_stops_are_equal = bus_ptr->first_and_last_stops_are_equal;
      
    for (const domain::Stop* stop_ptr : stops) {
      svg::Point stop = projector(stop_ptr->coordinates);
      active_stops_[stop_ptr->name] = stop;
      bus_data.route_points.emplace_back(
          std::pair(std::string_view(stop_ptr->name),std::move(stop)));
    }
    renderable_data_.emplace_back(std::move(bus_data));
  }
}

std::deque<map_renderer::BusDataToRender>
DataForRenderer::ExtractRenderableData() {
  return std::move(renderable_data_);
}

std::map<std::string_view, svg::Point, std::less<>>
DataForRenderer::ExtractActiveStops() {
  return std::move(active_stops_);
}

map_renderer::MapSettings DataForRenderer::ExtractSettings() {
  return std::move(settings_);
}

} // request_handler namespace
